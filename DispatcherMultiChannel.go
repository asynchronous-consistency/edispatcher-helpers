package edispatcher_helpers

import (
	"bitbucket.org/asynchronous-consistency/edispatcher/v3"
	"context"
	"github.com/pkg/errors"
	"golang.org/x/sync/errgroup"
)

// Множественный канал для работы с шиной. Предоставляет функционал для
// множественной доставки/приема событий.
type dispatcherMultiChannel struct {
	dispatchers innerDispatchers
}

// Dispatch выполняет доставку событий в шину
func (d dispatcherMultiChannel) Dispatch(ctx context.Context, eventsData []string) error {
	if ctx == nil {
		ctx = context.Background()
	}

	// Рабочий контекст. Служит для остановки рабочих групп
	g, wCtx := errgroup.WithContext(ctx)

	// Пересылаем событие во все каналы, для которых разрешена доставка
	for i := range d.dispatchers {
		dispatcher := d.dispatchers[i]

		if !dispatcher.dispatcher.Dispatching {
			continue
		}

		g.Go(func() error {
			return dispatcher.dispatcher.Channel.Dispatch(wCtx, eventsData)
		})
	}

	return g.Wait()
}

// Receive выполняет получение событий из шины
func (d dispatcherMultiChannel) Receive(ctx context.Context, lastEvent string, events chan *edispatcher.EventTemporaryData) error {
	if ctx == nil {
		ctx = context.Background()
	}

	// Рабочий контекст. Служит для остановки рабочих групп
	g, wCtx := errgroup.WithContext(ctx)

	for i := range d.dispatchers {
		if !d.dispatchers[i].dispatcher.Receiving {
			continue
		}

		dispatcher := d.dispatchers[i]
		channel := make(chan *edispatcher.EventTemporaryData, dispatcher.dispatcher.BufferSize)

		// Запускаем копирование из канала, открытого для чтения
		g.Go(func() error {
			for {
				select {
				case <-wCtx.Done():
					for 0 != len(channel) {
						<-channel
					}

					return nil
				case msg := <-channel:
					origin := multiChannelMessage{
						UUID:   dispatcher.uuid,
						Origin: msg,
					}

					events <- &edispatcher.EventTemporaryData{
						Event:  msg.Event,
						Origin: origin,
					}
				}
			}
		})

		g.Go(func() error {
			defer close(channel)

			return dispatcher.dispatcher.Channel.Receive(wCtx, lastEvent, channel)
		})
	}

	err := g.Wait()
	if nil != err {
		return errors.Wrap(err, `failed to receive events from multichannel`)
	}

	return err
}
