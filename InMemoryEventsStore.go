package edispatcher_helpers

import (
	"bitbucket.org/asynchronous-consistency/edispatcher/v3"
	"bitbucket.org/go-generics/h"
	"context"
	"github.com/google/uuid"
	"sync"
)

// InMemory вариант хранилища данных для EDispatcher
type inMemoryEventsStore[Tx any] struct {
	events []*edispatcher.StoredEventData
	mx     sync.Mutex
}

// CreateEvents выполняет создание событий для доставки в шину
func (i *inMemoryEventsStore[Tx]) CreateEvents(_ context.Context, data []string, _ Tx) error {
	i.mx.Lock()
	defer i.mx.Unlock()

	lastIndex := 0
	events := make([]*edispatcher.StoredEventData, len(i.events)+len(data))
	for _, event := range i.events {
		events[lastIndex] = event
		lastIndex++
	}

	for _, eventData := range data {
		events[lastIndex] = &edispatcher.StoredEventData{
			Id:   uuid.New().String(),
			Data: eventData,
		}
		lastIndex++
	}

	i.events = events

	return nil
}

// LoadEvents загружает события, сохраненные для доставки в шину
func (i *inMemoryEventsStore[Tx]) LoadEvents(context.Context, Tx) ([]*edispatcher.StoredEventData, error) {
	i.mx.Lock()
	defer i.mx.Unlock()

	result := make([]*edispatcher.StoredEventData, len(i.events))
	for index := range i.events {
		result[index] = i.events[index]
	}

	return result, nil
}

// DeleteEvents удаляет события, отправленные в шину
func (i *inMemoryEventsStore[Tx]) DeleteEvents(_ context.Context, eventIds []string, _ Tx) error {
	i.mx.Lock()
	defer i.mx.Unlock()

	events := make([]*edispatcher.StoredEventData, 0, len(i.events))
	for _, event := range i.events {
		if !h.InArray(event.Id, eventIds) {
			events = append(events, event)
		}
	}

	i.events = events

	return nil
}
