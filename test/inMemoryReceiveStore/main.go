package main

import (
	"bitbucket.org/asynchronous-consistency/edispatcher-helpers/v3"
	"log"
)

func main() {
	store := edispatcher_helpers.NewInMemoryReceiveStore[any]()

	log.Println(store.GetLastReceivedEventId(nil, nil))

	_ = store.SetLastReceivedEventId(nil, "123", nil)
	log.Println(store.GetLastReceivedEventId(nil, nil))
}
