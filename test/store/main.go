package main

import (
	"bitbucket.org/asynchronous-consistency/edispatcher-helpers/v3"
	"log"
)

func main() {
	store := edispatcher_helpers.NewDispatcherMultiEventStore[any](
		edispatcher_helpers.Store[any]{
			Store:    edispatcher_helpers.NewInMemoryEventsStore[any](),
			Creating: false,
			Loading:  true,
			Deleting: true,
		},
		edispatcher_helpers.Store[any]{
			Store:    edispatcher_helpers.NewInMemoryEventsStore[any](),
			Creating: true,
			Loading:  false,
			Deleting: true,
		},
		edispatcher_helpers.Store[any]{
			Store:    edispatcher_helpers.NewInMemoryEventsStore[any](),
			Creating: true,
			Loading:  true,
			Deleting: false,
		},
		edispatcher_helpers.Store[any]{
			Store:    edispatcher_helpers.NewInMemoryEventsStore[any](),
			Creating: true,
			Loading:  false,
			Deleting: false,
		},
		edispatcher_helpers.Store[any]{
			Store:    edispatcher_helpers.NewInMemoryEventsStore[any](),
			Creating: true,
			Loading:  true,
			Deleting: true,
		},
	)

	events, _ := store.LoadEvents(nil, nil)
	log.Println(len(events)) // Должно быть 0

	_ = store.CreateEvents(nil, []string{"1", "2"}, nil) // Должны прийти в #2, 3, 4, 5

	events, _ = store.LoadEvents(nil, nil) // Должно быть 4 из #3, 5
	log.Println(len(events))

	var eventIds []string
	for _, e := range events {
		eventIds = append(eventIds, e.Id)
	}

	_ = store.DeleteEvents(nil, eventIds, nil)

	events, _ = store.LoadEvents(nil, nil)
	log.Println(len(events)) // Должно быть 2 из #3
}
