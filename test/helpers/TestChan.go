package helpers

import (
	"bitbucket.org/asynchronous-consistency/edispatcher/v3"
	"bitbucket.org/go-generics/h"
	"context"
	"fmt"
	"log"
	"strings"
)

// Тестовый канал для отправки уведомлений
type TestChan struct {
	Name string
}

// Доставка событий в шину
func (t TestChan) Dispatch(_ context.Context, eventsData []string) error {
	log.Println(fmt.Sprintf(`Dispatching (%v) -> %v`, t.Name, strings.Join(eventsData, ", ")))

	return nil
}

// Получение событий из шины в очередь
func (t TestChan) Receive(ctx context.Context, _ string, events chan *edispatcher.EventTemporaryData) error {
	i := 0

	for !h.IsCtxDone(ctx) {
		i++
		events <- &edispatcher.EventTemporaryData{
			Event: edispatcher.StoredEventData{
				Id:   t.Name,
				Data: fmt.Sprintf(`%v`, i),
			},
			Origin: nil,
		}
	}

	return nil
}
