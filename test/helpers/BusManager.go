package helpers

import (
	"bitbucket.org/asynchronous-consistency/edispatcher/v3"
	"context"
	"fmt"
	"log"
)

// Подставка для тестирования
type BusManager struct {
	Name string
}

// Подтверждение принятия события на стороне шины
func (b BusManager) CommitEvent(_ context.Context, event *edispatcher.EventTemporaryData) error {
	log.Println(fmt.Sprintf(`Commit (%v) -> %v`, b.Name, event.Event.Id))

	return nil
}

// Отклонение принятия события на стороне шины
func (b BusManager) RejectEvent(context.Context, *edispatcher.EventTemporaryData) error {
	return nil
}
