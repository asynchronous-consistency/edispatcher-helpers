package main

import (
	"bitbucket.org/asynchronous-consistency/edispatcher-helpers/v3"
	"bitbucket.org/asynchronous-consistency/edispatcher-helpers/v3/test/helpers"
	"bitbucket.org/asynchronous-consistency/edispatcher/v3"
	"bitbucket.org/go-generics/h"
	"context"
	"log"
	"time"
)

func main() {
	channel, manager := edispatcher_helpers.NewDispatcherMultiChannel(
		edispatcher_helpers.Dispatcher{
			Channel:     &helpers.TestChan{Name: "test-1"},
			BusManager:  &helpers.BusManager{Name: "test-1"},
			Receiving:   true,
			Dispatching: true,
			BufferSize:  2,
		},
		edispatcher_helpers.Dispatcher{
			Channel:     &helpers.TestChan{Name: "test-2"},
			BusManager:  &helpers.BusManager{Name: "test-2"},
			Receiving:   true,
			Dispatching: true,
			BufferSize:  2,
		},
		edispatcher_helpers.Dispatcher{
			Channel:     &helpers.TestChan{Name: "test-3"},
			BusManager:  &helpers.BusManager{Name: "test-3"},
			Receiving:   true,
			Dispatching: false,
			BufferSize:  2,
		},
	)

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	events := make(chan *edispatcher.EventTemporaryData, 100)
	go func() {
		_ = channel.Receive(ctx, "-1", events)
	}()

	for !h.IsCtxDone(ctx) {
		msg := <-events
		log.Println(msg)

		_ = manager.CommitEvent(ctx, msg)
		time.Sleep(100 * time.Millisecond)
	}
}
