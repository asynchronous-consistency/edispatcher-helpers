package main

import (
	"bitbucket.org/asynchronous-consistency/edispatcher-helpers/v3"
	"bitbucket.org/asynchronous-consistency/edispatcher-helpers/v3/test/helpers"
	"context"
	"fmt"
	"log"
)

func main() {
	channel, _ := edispatcher_helpers.NewDispatcherMultiChannel(
		edispatcher_helpers.Dispatcher{
			Channel:     &helpers.TestChan{Name: "test-1"},
			BusManager:  nil,
			Receiving:   true,
			Dispatching: true,
			BufferSize:  100,
		},
		edispatcher_helpers.Dispatcher{
			Channel:     &helpers.TestChan{Name: "test-2"},
			BusManager:  nil,
			Receiving:   true,
			Dispatching: true,
			BufferSize:  100,
		},
		edispatcher_helpers.Dispatcher{
			Channel:     &helpers.TestChan{Name: "test-3"},
			BusManager:  nil,
			Receiving:   true,
			Dispatching: false,
			BufferSize:  100,
		},
	)

	ctx := context.Background()
	messages := make([]string, 200)
	for i := 0; i < 200; i++ {
		messages[i] = fmt.Sprintf(`%v`, i)
	}

	log.Fatal(channel.Dispatch(ctx, messages))
}
