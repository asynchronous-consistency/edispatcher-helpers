package main

import (
	"bitbucket.org/asynchronous-consistency/edispatcher-helpers/v3"
	"bitbucket.org/asynchronous-consistency/edispatcher/v3"
	"bitbucket.org/go-generics/h"
	"log"
)

func main() {
	store := edispatcher_helpers.NewInMemoryEventsStore[any]()

	_ = store.CreateEvents(nil, []string{"1", "2", "3"}, nil)

	events, _ := store.LoadEvents(nil, nil)
	log.Println(events)

	eventIds := h.Map(events, func(event *edispatcher.StoredEventData) string {
		return event.Id
	})

	_ = store.DeleteEvents(nil, eventIds, nil)
	log.Println(store.LoadEvents(nil, nil))
}
