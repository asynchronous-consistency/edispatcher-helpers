module bitbucket.org/asynchronous-consistency/edispatcher-helpers/v3

go 1.18

require (
	bitbucket.org/asynchronous-consistency/edispatcher/v3 v3.0.0
	bitbucket.org/go-generics/h v0.9.1
	github.com/google/uuid v1.3.0
	github.com/pkg/errors v0.9.1
	golang.org/x/sync v0.1.0
)

require (
	bitbucket.org/sveshnikovwork/multiservice/v3 v3.0.2 // indirect
	github.com/andybalholm/brotli v1.0.4 // indirect
	github.com/klauspost/compress v1.15.12 // indirect
	github.com/sirupsen/logrus v1.9.0 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasthttp v1.43.0 // indirect
	github.com/xlab/closer v1.1.0 // indirect
	golang.org/x/net v0.4.0 // indirect
	golang.org/x/sys v0.3.0 // indirect
	gopkg.in/tomb.v2 v2.0.0-20161208151619-d5d1b5820637 // indirect
)
