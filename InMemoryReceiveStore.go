package edispatcher_helpers

import (
	"bitbucket.org/go-generics/h"
	"context"
)

// InMemory хранилище для текущего события
type inMemoryReceiveStore[Tx any] struct {
	eventId *h.SyncValueStruct[string]
}

// GetLastReceivedEventId возвращает ID последнего принятого события
func (i *inMemoryReceiveStore[Tx]) GetLastReceivedEventId(_ context.Context, _ Tx) (string, error) {
	return i.eventId.Get(), nil
}

// SetLastReceivedEventId устанавливает ID последнего принятого события
func (i *inMemoryReceiveStore[Tx]) SetLastReceivedEventId(_ context.Context, eventId string, _ Tx) error {
	i.eventId.Set(eventId)

	return nil
}
