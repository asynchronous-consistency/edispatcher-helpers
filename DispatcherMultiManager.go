package edispatcher_helpers

import (
	"bitbucket.org/asynchronous-consistency/edispatcher/v3"
	"context"
)

// Сервис для работы с шиной. Необходим для того, чтоб с исходным событием работала
// та шина, из которой событие пришло.
type dispatcherMultiManager struct {
	dispatchers innerDispatchers
}

// CommitEvent подтверждает принятие события на стороне шины
func (d dispatcherMultiManager) CommitEvent(ctx context.Context, event *edispatcher.EventTemporaryData) error {
	origin := event.Origin.(multiChannelMessage)

	for _, dispatcher := range d.dispatchers {
		if dispatcher.uuid != origin.UUID {
			continue
		}

		return dispatcher.dispatcher.BusManager.CommitEvent(ctx, &edispatcher.EventTemporaryData{
			Event:  event.Event,
			Origin: origin.Origin,
		})
	}

	return nil
}

// RejectEvent отклоняет событие на стороне шины
func (d dispatcherMultiManager) RejectEvent(ctx context.Context, event *edispatcher.EventTemporaryData) error {
	origin := event.Origin.(multiChannelMessage)

	for _, dispatcher := range d.dispatchers {
		if dispatcher.uuid != origin.UUID {
			continue
		}

		return dispatcher.dispatcher.BusManager.RejectEvent(ctx, &edispatcher.EventTemporaryData{
			Event:  event.Event,
			Origin: origin.Origin,
		})
	}

	return nil
}
