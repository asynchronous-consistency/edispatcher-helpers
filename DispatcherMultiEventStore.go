package edispatcher_helpers

import (
	"bitbucket.org/asynchronous-consistency/edispatcher/v3"
	"context"
	"fmt"
	"strings"
)

// Мультихранилище для событий
type dispatcherMultiEventStore[Tx any] struct {
	stores []innerStore[Tx]
}

// CreateEvents выполняет создание событий для доставки в шину
func (d dispatcherMultiEventStore[Tx]) CreateEvents(ctx context.Context, data []string, tx Tx) error {
	for _, store := range d.stores {
		if !store.Store.Creating {
			continue
		}

		err := store.Store.Store.CreateEvents(ctx, data, tx)
		if nil != err {
			return err
		}
	}

	return nil
}

// LoadEvents загружает события, сохраненные для доставки в шину
func (d dispatcherMultiEventStore[Tx]) LoadEvents(ctx context.Context, tx Tx) ([]*edispatcher.StoredEventData, error) {
	var result []*edispatcher.StoredEventData

	for _, store := range d.stores {
		if !store.Store.Loading {
			continue
		}

		events, err := store.Store.Store.LoadEvents(ctx, tx)
		if nil != err {
			return nil, err
		}

		newEvents := make([]*edispatcher.StoredEventData, len(result)+len(events))
		lastIndex := 0

		for _, event := range result {
			newEvents[lastIndex] = event
			lastIndex++
		}

		// Добавляем к событию ID стора, из которого событие получено.
		for _, event := range events {
			newEvents[lastIndex] = &edispatcher.StoredEventData{
				Id:   fmt.Sprintf("%v---%v", store.Uuid, event.Id),
				Data: event.Data,
			}
			lastIndex++
		}

		result = newEvents
	}

	return result, nil
}

// DeleteEvents удаляет события, отправленные в шину
func (d dispatcherMultiEventStore[Tx]) DeleteEvents(ctx context.Context, eventIds []string, tx Tx) error {
	for _, store := range d.stores {
		if !store.Store.Deleting {
			continue
		}

		// Удаляем ID хранилища из ID события. Это необходимо для того, чтоб хранилища принимали
		// только свои реальные идентификаторы.
		eventsToRemove := make([]string, 0, len(eventIds))
		for _, id := range eventIds {
			// Если событие не из этого стора, то нечего удалять
			if !strings.Contains(id, store.Uuid+"---") {
				continue
			}

			eventsToRemove = append(eventsToRemove, strings.Replace(id, store.Uuid+"---", "", 1))
		}

		err := store.Store.Store.DeleteEvents(ctx, eventsToRemove, tx)
		if nil != err {
			return err
		}
	}

	return nil
}
