package edispatcher_helpers

import (
	"bitbucket.org/asynchronous-consistency/edispatcher/v3"
	"bitbucket.org/go-generics/h"
	"github.com/google/uuid"
	"sync"
)

// NewDispatcherMultiChannel реализует фабрику канала с множественной доставкой событий
func NewDispatcherMultiChannel(
	dispatchers ...Dispatcher,
) (channel edispatcher.EventChannelInterface, manager edispatcher.BusEventManagerInterface) {
	inner := h.Map(dispatchers, func(dispatcher Dispatcher) innerDispatcher {
		return innerDispatcher{
			dispatcher: dispatcher,
			uuid:       uuid.New().String(),
		}
	})

	channel = dispatcherMultiChannel{dispatchers: inner}
	manager = dispatcherMultiManager{dispatchers: inner}

	return
}

// NewInMemoryReceiveStore реализует фабрику InMemory хранилища указателя на события
func NewInMemoryReceiveStore[Tx any]() edispatcher.EventsReceiveStoreInterface[Tx] {
	return &inMemoryReceiveStore[Tx]{
		eventId: h.SyncValue[string]("-1"),
	}
}

// NewInMemoryEventsStore реализует фабрику InMemory хранилища событий для EDispatcher
func NewInMemoryEventsStore[Tx any]() edispatcher.EventsStoreInterface[Tx] {
	return &inMemoryEventsStore[Tx]{
		events: nil,
		mx:     sync.Mutex{},
	}
}

// NewDispatcherMultiEventStore реализует фабрику store с множественным сохранением/загрузкой событий
func NewDispatcherMultiEventStore[Tx any](stores ...Store[Tx]) edispatcher.EventsStoreInterface[Tx] {
	inner := h.Map(stores, func(store Store[Tx]) innerStore[Tx] {
		return innerStore[Tx]{
			Store: store,
			Uuid:  uuid.New().String(),
		}
	})

	return &dispatcherMultiEventStore[Tx]{
		stores: inner,
	}
}

// NewEmptyTxManager реализует фабрику пустого менеджера транзакций
func NewEmptyTxManager[Tx any]() edispatcher.TransactionManagerInterface[Tx] {
	return &emptyTxManager[Tx]{}
}
