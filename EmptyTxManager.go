package edispatcher_helpers

import (
	"context"
)

// Заглушка для менеджера транзакций.
type emptyTxManager[Tx any] struct{}

// Begin - Начало транзакции
func (e emptyTxManager[Tx]) Begin(context.Context) (Tx, error) {
	var noTx Tx

	return noTx, nil
}

// Rollback - Откат изменений
func (e emptyTxManager[Tx]) Rollback(_ context.Context, _ Tx) {
}

// Commit - Коммит изменений
func (e emptyTxManager[Tx]) Commit(context.Context, Tx) {
}
