package edispatcher_helpers

import "bitbucket.org/asynchronous-consistency/edispatcher/v3"

// Dispatcher описывает сборный тип для описания связки диспетчера и менеджера очереди
type Dispatcher struct {
	Channel     edispatcher.EventChannelInterface    // Канал для работы с шиной
	BusManager  edispatcher.BusEventManagerInterface // Менеджер шины
	Receiving   bool                                 // Требуется ли прием событий из этого канала
	Dispatching bool                                 // Требуется ли доставка событий в этот канал
	BufferSize  uint16                               // Размер буфера для канала
}

// Сборный тип для описания связки диспетчера и менеджера очереди
type innerDispatcher struct {
	dispatcher Dispatcher
	uuid       string
}

// Сообщение из множественного канала
type multiChannelMessage struct {
	UUID   string
	Origin interface{}
}

// Dispatchers - Коллекция диспетчеров
type Dispatchers = []Dispatcher

// Коллекция диспетчеров для внутреннего использования
type innerDispatchers = []innerDispatcher
