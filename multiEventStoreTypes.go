package edispatcher_helpers

import "bitbucket.org/asynchronous-consistency/edispatcher/v3"

// Store описывает сборный тип для описания связки диспетчера и менеджера очереди
type Store[Tx any] struct {
	Store    edispatcher.EventsStoreInterface[Tx] // Хранилище для работы с событиями
	Creating bool                                 // Требуется ли создавать событие в сторе
	Loading  bool                                 // Требуется ли загружать события из стора
	Deleting bool                                 // Требуется ли удалять события в сторе
}

// Внутренний store с идентификаторами
type innerStore[Tx any] struct {
	Store Store[Tx] // Данные хранилища
	Uuid  string    // Уникальный идентификатов хранилища
}
